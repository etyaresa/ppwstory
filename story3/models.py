from django.db import models

class Matkul(models.Model):
     nama_mata_kuliah      = models.CharField(max_length = 50)
     dosen_pengajar        = models.CharField(max_length = 50)
     jumlah_sks            = models.IntegerField()
     deskripsi_mata_kuliah = models.CharField(max_length = 150)
     semester_tahun        = models.CharField(max_length = 15)
     kelas                 = models.CharField(max_length = 15)

     def __str__(self):
        return "{}.{}".format(self.id, self.nama_mata_kuliah)
