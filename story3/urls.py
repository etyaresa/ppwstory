from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('exp/', views.exp, name = 'exp'),
    path('contact/', views.kontak, name = 'kontak'),
    path('matkul/', views.buatmatkul, name = 'matkul'),
    path('matkul/create', views.buatmatkul, name = 'create'),
    path('matkul/data', views.posmatkul, name = 'data'),
    path('matkul/data/delete/(?<delete_id>[0-9]+)', views.delete, name = 'delete'),
]