from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'nama_mata_kuliah',
            'dosen_pengajar',
            'jumlah_sks',
            'deskripsi_mata_kuliah',
            'semester_tahun',
            'kelas'
        ]

        labels = {
            'nama_mata_kuliah':'Course',
            'dosen_pengajar': 'Lecturer',
            'jumlah_sks':'SKS',
            'deskripsi_mata_kuliah':'Description',
            'semester_tahun':'Semester',
            'kelas':'Classroom'
        }
        
        widgets = {
            'nama_mata_kuliah': forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'enter course name '
                }
            ),
            'dosen_pengajar': forms.TextInput(
                attrs={
                'class':'form-control',
                'placeholder':'enter lecturer'
            }
            ),
            'jumlah_sks': forms.TextInput(
                attrs={
                'class':'form-control',
                'placeholder':'4'
            }
            ),
            'deskripsi_mata_kuliah': forms.Textarea(
                attrs={
                    'class':'form-control',
                    'placeholder':''
                }
            ),
            'semester_tahun':forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Gasal 2020/2021'
                }
            ),
            'kelas' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'2.2303 '
                }
            )
        }

