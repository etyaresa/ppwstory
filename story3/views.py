from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import MatkulForm
from .models import Matkul

def index(request):
    return render(request, 'home/index.html')

def exp(request):
    return render(request, 'exp/exp.html')

def kontak(request):
    return render(request, 'kontak/kontak.html')

def buatmatkul(request):
    form_field = MatkulForm(request.POST or None)
    if request.method == 'POST':
        if form_field.is_valid():
            form_field.save()
        return HttpResponseRedirect("/matkul/data")
    cotext = {
        'data_form':form_field,
    }
    return render(request, 'matkul/create.html', cotext)

def posmatkul(request):
    posts = Matkul.objects.all()
    context = {
        'posts':posts
    }
    return render(request, 'matkul/data.html', context)

def delete(request,delete_id):
    Matkul.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("/matkul/data")


