$(document).ready(function () {
    $('.accordion-toggle').on('click', function(event){
      event.preventDefault();

       if($(this).hasClass("active")){
          $(this).removeClass("active");
          $(this)
            .siblings(".accordion-content")
            .slideUp(100);
       }
       else {
          $("accordion-toggle").removeClass("active");
          $(this).addClass("active");
          $(".accordion-content").slideUp(100);
          $(this)
            .siblings(".accordion-content")
            .slideDown(100);
        }

    });

    
  $('.up').click(function() {
        var item = $(this).parents('div.accordion-container'),
            swapWith = item.prev();
        item.after(swapWith);
    });
  
    $('.down').click(function() {
        var item = $(this).parents('div.accordion-container'),
            swapWith = item.next();
        item.before(swapWith);
    });

});