from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .import views

class TestStory7(TestCase):
    def test_url_story7_ada(self):
        response=Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_story7_ada(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'new.html')

    def test_fungsi_story7_ada(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.index)