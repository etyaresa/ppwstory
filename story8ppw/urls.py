from django.urls import path
from . import views

app_name = 'story8ppw'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('data/', views.fdata, name = 'fdata'),
]