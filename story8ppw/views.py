from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json


def index(request):
    return render(request, 'book.html')

def fdata(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)

# Create your views here.
