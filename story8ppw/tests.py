from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .import views

# Create your tests here.
class TestStory8(TestCase):
    def test_url_story8_ada(self):
        response=Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_story8_ada(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'book.html')

    def test_fungsi_story8_ada(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, views.index)
  

