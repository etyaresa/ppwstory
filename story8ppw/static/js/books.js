$(document).ready(function () {

    //default
    $.ajax({
        url: 'data?q=doraemon', 
       
        success: function(data){
           
            var array_items = data.items;
            $("#daftar_isi").empty();            

            for(i = 0; i< array_items.length; i++ ){
                var judul = array_items[i].volumeInfo.title;
                var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                var author = array_items[i].volumeInfo.authors[0];
                var penerbit = array_items[i].volumeInfo.publisher;
               
                $('#daftar_isi').append(
                    "<tr>" +
                    "<th> Book" + "</th>" +
                    "<th> Tittle" + "</th>" +
                    "<th> Author" + "</th>" +
                    "<th > Publisher" + "</th>" +
                    "</tr>" +
                    "<tr>" +
                    "<td class = gambar>" +"<img width=75% src= " + gambar+ "/>"+ "</td>" +
                    "<td class = judul >" + judul + "</td>" +
                    "<td class = tulisan>" +  author +  "</td>" +
                    "<td class = tulisan>" + penerbit+  "</td>" +
                    "</tr>" 
                );
            }
        }
    });



    $("#keyword").keyup( function(){
        var ketikan = $("#keyword").val();
       
        //panggil ajax
        $.ajax({
            url: 'data?q=' + ketikan, 
           
            success: function(data){
               
                var array_items = data.items;
                $("#daftar_isi").empty();            

                for(i = 0; i< array_items.length; i++ ){
                    var judul = array_items[i].volumeInfo.title;
                    var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var author = array_items[i].volumeInfo.authors[0];
                    var penerbit = array_items[i].volumeInfo.publisher;
                   
                    $('#daftar_isi').append(
                        "<tr>" +
                        "<th> Book" + "</th>" +
                        "<th> Tittle" + "</th>" +
                        "<th> Author" + "</th>" +
                        "<th > Publisher" + "</th>" +
                        "</tr>" +
                        "<tr>" +
                        "<td class = gambar>" +"<img width=75% src= " + gambar+ "/>"+ "</td>" +
                        "<td class = judul >" + judul + "</td>" +
                        "<td class = tulisan>" +  author +  "</td>" +
                        "<td class = tulisan>" + penerbit+  "</td>" +
                        "</tr>" 
					);
                }
            }
        });
    });

});