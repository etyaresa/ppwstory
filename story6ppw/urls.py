from django.urls import path

from .import views

app_name = 'story6'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('create/', views.addKegiatan, name = 'addKegiatan'),
    path('join/<int:p_id>', views.joinPeserta, name = 'joinPeserta')
]
