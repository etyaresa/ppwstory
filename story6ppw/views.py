from django.shortcuts import render, redirect
from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan, Peserta

# Create your views here.
def index(request):
    pos_kegiatan = Kegiatan.objects.all()
    context = {
        'pos':pos_kegiatan
    }
    return render(request, 'index.html', context)

def addKegiatan(request):
    Kegiatan_form = KegiatanForm(request.POST or None)
    if request.method == 'POST':
        if Kegiatan_form.is_valid():
            Kegiatan_form.save()
            return redirect("/story6/")

    context = {
        'data_form' : Kegiatan_form,
    }
    return render(request, 'addK.html', context)

def joinPeserta(request, p_id):
    default = Kegiatan.objects.get(pk = p_id)
    Peserta_form = PesertaForm(request.POST or None, initial={'kegiatan':default})
    if request.method == 'POST':
        if Peserta_form.is_valid():
            Peserta_form.save()
            return redirect("/story6/")

    context = {
        'data_form' : Peserta_form,
    }
    return render(request, 'join.html', context)
   


