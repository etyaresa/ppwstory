from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import Kegiatan, Peserta
from .import views

# Create your tests here.
class TestStory6(TestCase):
    def test_url_story6_ada(self):
        response=Client().get('/story6/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_story6_ada(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'index.html')

    def test_fungsi_story6_ada(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, views.index)

    def test_url_create_kegiatan_ada(self):
        response=Client().get('/story6/create/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_create_kegiatan_ada(self):
        response = Client().get('/story6/create/')
        self.assertTemplateUsed(response, 'addK.html')

    def test_fungsi_create_kegiatan_ada(self):
        found = resolve('/story6/create/')
        self.assertEqual(found.func, views.addKegiatan)

    def test_url_join_peserta_ada(self):
        url= reverse('story6:joinPeserta', args=[1])
        self.assertEqual(url,'/story6/join/1')
    
    def test_template_join_peserta_ada(self):
        a = Kegiatan.objects.create(nama='Kegiatan_test')
        response = Client().get('/story6/join/'+str(a.id))
        self.assertTemplateUsed(response, 'join.html')

    def test_fungsi_join_peserta_ada(self):
        found = resolve('/story6/join/1')
        self.assertEqual(found.func, views.joinPeserta)

    def test_model_kegiatan_ada(self):
        Kegiatan.objects.create(nama='Kegiatan_test')
        total = Kegiatan.objects.all().count()
        self.assertEqual(total,1)
    
    def test_story6_save_kegiatan_POST(self):
        Client().post('/story6/create/', data={'nama': 'K1'})
        jumlah = Kegiatan.objects.filter(nama='K1').count()
        self.assertEqual(jumlah, 1)

    def test_model_join_peserta_ada(self):
        a = Kegiatan.objects.create(nama='Kegiatan_test')
        Peserta.objects.create(nama="lalala", kegiatan = a)
        total = Peserta.objects.all().count()
        self.assertEqual(total,1)
    
    def test_di_halaman_create_kegiatan_ada_tombol_Create_dan_Create_Activities(self):
        response = Client().get('/story6/create/',{'nama':'Create Activities'})
        html_create = response.content.decode('utf8')
        self.assertIn("Create", html_create)
        self.assertIn("Create Activities", html_create)

    def test_str_model_sama_dengan_field_kegiatan(self):
        Kegiatan.objects.create(nama = "a")
        nama = Kegiatan.objects.get(pk = 1)
        self.assertEqual(str(nama), nama.nama)

    def test_story6_kegiatan_form_invalid(self):
        Client().post('/story6/create/', data={})
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 0)
  

    def test_story6_save_peserta_a_POST_request(self):
        obj = Kegiatan.objects.create(nama='Unit Test2')
        a = Peserta.objects.create(nama = "b", kegiatan=obj)
        obj.save()
        Client().post('/story6/' + str(obj.id), data={'nama': 'b'})
        jumlah = Peserta.objects.filter(nama='b', pk = obj.id).count()
        self.assertEqual(jumlah, 1)
        

    def test_str_model_pesert(self):
        obj = Kegiatan.objects.create(nama='Unit Test2')
        Peserta.objects.create(nama = "a", kegiatan=obj)
        nama = Peserta.objects.get(pk = 1)
        self.assertEqual(str(nama), nama.nama)