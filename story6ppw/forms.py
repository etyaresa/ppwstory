from django import forms
from .models import Kegiatan, Peserta

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan 
        fields = ['nama']
        label = {'nama' : 'Activities'}
        widgets = {
            'nama': forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'enter type of activity'
                }
            )
        }

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'
        # fields = ['nama']
        # label = {'nama' : 'Name'}
        # widgets = {
        #     'nama': forms.TextInput(
        #         attrs={
        #             'class':'form-control',
        #             'placeholder':'enter type of activity'
        #         }
        #     )
        # }