from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self):
         return self.nama

class Peserta(models.Model):
    nama = models.CharField(max_length=50)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE) 

    def __str__(self):
        return self.nama


#1 kegiatan punya byk orang, 1 org cuma 1 kegiatan