from django.urls import path
from . import views

app_name = 'story9'
urlpatterns = [
    path('', views.loginUser, name = 'loginUser'),
    path('logout/', views.logoutUser, name='logoutUser'),
    path('register/', views.register, name = 'register'),
    path('halo/',views.halo, name='halo'),
]