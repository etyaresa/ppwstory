from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from .views import register, loginUser, logoutUser, halo
from django.http import HttpRequest
from .forms import CreatUserForm
from django.contrib.sessions.backends.db import SessionStore
from django.contrib import messages

class testAccount(TestCase):
    def test_url_login_ada(self):
        response = Client().get('/story9/login/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_login_ada_templatenya(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_login_views(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, loginUser)

    def test_url_register_ada(self):
        response = Client().get('/story9/login/register/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_register_ada_templatenya(self):
        response = Client().get('/story9/login/register/')
        self.assertTemplateUsed(response, 'register.html')
    
    def test_regiter_views_ada(self):
        found = resolve('/story9/login/register/')
        self.assertEqual(found.func, register)
    
    def test_url_halo_page_ada(self):
        response = Client().get('/story9/login/halo/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_halopage_ada_templatenya(self):
        response = Client().get('/story9/login/halo/')
        self.assertTemplateUsed(response, 'halo.html')
    
    def test_halo_views_ada(self):
        found = resolve('/story9/login/halo/')
        self.assertEqual(found.func, halo)
    
    def test_url_logout_ada(self):
        response = Client().get('/story9/login/')
        self.assertEquals(response.status_code, 200)

    def test_logout_views_ada(self):
        found = resolve('/story9/login/logout/')
        self.assertEqual(found.func, logoutUser)

    def test_register(self):
        Client().post('/story9/login/register/', data={'username' : 'aa','password1' : 'aabbccdd123','password2' : 'aabbccdd123'})
        jumlah = User.objects.filter(username='aa').count()
        self.assertEqual(jumlah, 1)
    
    def test_login(self):
        request = HttpRequest() 
        request.session = SessionStore(session_key=None)
        request.method = 'POST'
        response = loginUser(request)
        self.assertEquals(response.status_code, 200)
    
    def test_logout(self):
        request = HttpRequest()
        request.session = SessionStore(session_key=None)
        response = logoutUser(request)
        self.assertEquals(response.status_code, 302)

  


    

    

    




