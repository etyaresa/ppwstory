from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .forms import CreatUserForm
from django.contrib import messages

def register(request):
    form = CreatUserForm()
    if request.method == 'POST':
        form = CreatUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('story9:loginUser')
    context ={'form':form}
    return render(request, 'register.html', context)

def loginUser(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('story9:halo')
    return render(request, 'login.html')

def logoutUser(request):
    logout(request)
    return redirect('story9:loginUser')

def halo(request):
    return render(request, 'halo.html')